﻿namespace one;

public class Anagram
{
    public void AnagramMethod()
    {
        Console.Write("Enter text\t");
        string text = Console.ReadLine();

        Console.WriteLine(Revers(text));
    }

    private string Revers(string inputText)
    {
        if (inputText == null) 
        {
           throw new ArgumentNullException(); 
        }

        string result = "";

        for (int i = 0; i < inputText.Split(' ').Length; i++)
        {
            result+= ReversWord(inputText.Split(' ')[i]) + " ";
        }
        return result;

    }

    private string ReversWord(string chars)
    {
        if (chars == null )
        {
            throw new ArgumentNullException();
        }

        char[] buferArray = new char[chars.Length];
        char[] result = new char[chars.Length];
        int count1 = chars.Length - 1;
        int count2 = chars.Length - 1;

        for (int i = 0; i < chars.Length; i++)
        {
            if (!char.IsLetter(chars[i]))
            {
                result[i] = chars[i];
            }
            else
            {
                buferArray[count1] = chars[i];
                count1--;
            }
        }
        for (int i = result.Length - 1; i >= 0; i--)
        {
            if (result[i] == 0)
            {
                result[i] = buferArray[count2];
                count2--;
            }
        }

        string resultString = new string(result);
        return resultString;
    }
}